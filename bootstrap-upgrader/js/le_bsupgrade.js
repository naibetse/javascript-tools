/***
* Description: Bootstrap classes replacement, ver. 2 to 3.
* Author: Estebian, naibetse@users.noreply.github.com - www.librevolucion.com
* License: GNU GPLv3 or later - https://gnu.org/licenses/gpl.html
* Latest modification on: 19 Mar. 2016
***/

/***
* Use jQuery, tested with version 1.11.2 - https://jquery.com/download 
**

/* Please, don't modify this source code unless you know what you are doing */

leQuery = jQuery.noConflict();
(function( $ ) {
	// Main function
	$( document ).ready( function() {
		// Bootstrap 2.X Classes
		var oldClasses = [
			'container-fluid',
			'row-fluid',
			'span',
			'offset',
			'brand',
			'hero-unit',
			'icon-',
			'btn-mini',
			'btn-small',
			'btn-large',
			'visible-phone',
			'visible-tablet',
			'visible-desktop',
			'hidden-phone',
			'hidden-tablet',
			'hidden-desktop',
			'input-small',
			'input-large',
			'input-prepend',
			'input-append',
			'add-on',
			'btn-navbar',
			'ul.inline',
		];
		// Bootstrap 3.X Classes
		var newClasses = [
			'container',
			'row',
			'col-md-',
			'col-md-offset-',
			'navbar-brand',
			'jumbotron',
			'glyphicon glyphicon-',
			'btn-xs',
			'btn-sm',
			'btn-lg',
			'visible-sm',
			'visible-md',
			'visible-lg',
			'hidden-sm',
			'hidden-md',
			'hidden-lg',
			'input-sm',
			'input-lg',
			'input-group',
			'input-group',
			'input-group-addon',
			'navbar-btn',
			'list-inline',
		];
		// Replace Classes
		lestrapReplace( oldClasses, newClasses );
		
		var findEle = [
			'button, input[type=button], button[type=submit], .button, .btn',
			'input[type=text], input[type=password],input[type=search], input[type=url], input[type=tel], input[type=email], input[type=number], textarea, select'
		];

		var addClass = [
			'btn-default btn',
			'form-control'
		];
	
		// Add Classess
		lestrapAdd( findEle, addClass );
	});

	// Replace oldClasses to newClasses
	function lestrapReplace( oldc, newc ) {
		// Verify length of both arrays, are equal? 
		if ( oldc.length == newc.length ) {
			for ( var i=0; i < oldc.length; i++ ) {
				switch ( oldc[i] ) {
					case 'span' :
					case 'offset' :
 						var col = 1;
						while ( col <= 12 ) {
							$( '.' + oldc[i] + col ).addClass( newc[i] + col ).removeClass( oldc[i] + col );
							col++;
						}
						break;
					default :
						$( '.' + oldc[i] ).addClass( newc[i] ).removeClass( oldc[i] );
						break;	
				}	
			}
		} else {
			alert( 'Error: ¡Arrays with different lengths!' );
		}
	}

	// Add CSS class for each object to search
	function lestrapAdd( finde, addc ) {
		// Verify length of both arrays, are equal? 
		if ( finde.length == addc.length ) {
			for ( var i=0; i < finde.length; i++ ) {
				$( finde[i] ).addClass( addc[i] );
			}
		} else {
			console.log( 'Error: ¡Arrays with different lengths!' );
		}
	}
})( leQuery );
