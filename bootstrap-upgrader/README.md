# BOOTSTRAP UPGRADER FROM 2.X to 3.X.

Have you problems upgrading between Bootstrap [http://getbootstrap.com/](http://getbootstrap.com/ "Bootstrap") versions?. 
This simple, but useful tool into a single file made with _[jQuery](http://jquery.com/ "jQuery")_ Framework could save your time!.

## How can i to test it?...

* Download both files: le_bootstrap_upgrade.js and test.html.
* Run test.html from your Web browser and Check their code.
* And add le_bsupgrade.js to your .html file.
