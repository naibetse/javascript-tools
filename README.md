# SOME JAVASCRIPT TOOLS

### Some scripts made with JavaScript that could be useful for you.

*All good things might start with a simple "Hello World!"*

# RESOURCES

* **BOOTSTRAP UPGRADER**: 
    Have you problems upgrading between Bootstrap [http://getbootstrap.com/](http://getbootstrap.com/ "Bootstrap") versions?. 
    This simple, but useful tool into a single file made with _[jQuery](http://jquery.com/ "jQuery")_ Framework could save your time. 
    [Try on!, click here!] (bootstrap-upgrader/ "Bootstrap 2.X to 3.X upgrader").
